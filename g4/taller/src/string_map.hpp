#include "string_map.h"
#include "vector"

template <typename T>
string_map<T>::string_map(){

    raiz = new Nodo();
    _size = 0;
    //vector<Nodo*> ret (256, nullptr);
    /*raiz -> siguientes = vector<Nodo*>(256, nullptr);
    raiz -> definicion = nullptr;*/
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {

    //if(raiz != nullptr)
    destruir(raiz);
    if(d.raiz != nullptr){
        raiz = new Nodo();
        if(d.raiz->definicion) raiz->definicion = new T(*(d.raiz->definicion));
        copiar(raiz, d.raiz->siguientes);
        //raiz->copiar(d.raiz->siguientes);
    } else {
        raiz = nullptr;
    }

    return *this;
    /*
    destruir(raiz);
    //raiz = new Nodo();
    eqAux(raiz, d.raiz);
     */
}

template<typename T>
void string_map<T>::copiar(Nodo* root, const vector<Nodo*>& aCopiar) {
    for (int i = 0; i < 256; i++) {
        if (aCopiar[i] != nullptr) {
            root->siguientes[i] = new Nodo();
            if (aCopiar[i]->definicion)
                root->siguientes[i]->definicion = new T(*(aCopiar[i]->definicion));
            copiar(root->siguientes[i], aCopiar[i]->siguientes);
        }
    }
}

template <typename T>
string_map<T>::~string_map(){
    //if(raiz != nullptr)
    {
        destruir(raiz);
    }
}

template <typename T>
void string_map<T>::destruir(Nodo* root)
{
    if (root != nullptr)
    {

        for(int i = 0; i < 256; i++)
        {
            if(root -> siguientes[i] != nullptr)
            {
                destruir(root -> siguientes[i]);
            }
        }

        if(root -> definicion != nullptr){
            delete (root -> definicion);
            root -> definicion = nullptr;
        }
    }
    delete (root); //no le gusta estos delete ???


    root = nullptr;

    //_size--;

}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if (raiz == NULL)
        return 0;

    Nodo* actual = raiz;
    int i = 0;


    while(i < clave.length() && actual -> siguientes[clave[i]] != nullptr)
    {
        actual = (actual->siguientes)[clave[i]];
        i++;
    }
    return (i == clave.length() && actual->definicion != NULL);
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* actual = raiz;
    for(int i = 0; i < clave.length(); i++)
    {
        actual = actual->siguientes[clave[i]];

    }
    return *(actual->definicion);
}

template <typename T>
T& string_map<T>::at(const string& clave){

    Nodo* actual = raiz;
    for(int i = 0; i < clave.length(); i++)
    {
        actual = actual->siguientes[clave[i]];

    }
    return *(actual->definicion);

}

template <typename T>
void string_map<T>::erase(const string& clave) {
    if(clave != ""){
        Nodo *actual = raiz;
        Nodo *ultimo_nodo = raiz;
        int pos = 0;

        for (int i = 0; i < clave.size(); i++) {
            int letra = int(clave[i]);
            actual = actual->siguientes[letra];
            if (tieneHijos(actual) || actual->definicion != nullptr) {
                ultimo_nodo = actual;
                pos = i;
            }
        }

        if(actual->definicion) {delete actual->definicion;
            actual->definicion = nullptr;}

        if (!tieneHijos(actual)) {
            int letra = int(clave[pos]);
            Nodo *siguiente = ultimo_nodo->siguientes[letra];
            ultimo_nodo = siguiente;
            pos++;
            while (pos < clave.size()) {
                int letra = int(clave[pos]);
                Nodo *siguiente = ultimo_nodo->siguientes[letra];
                destruir(ultimo_nodo);
                ultimo_nodo = siguiente;
                pos++;
            }
        }
    } else {
        destruir(raiz);/*
        if(raiz->definicion) {delete raiz->definicion;
            raiz->definicion = nullptr;}
        if(!tieneHijos(raiz)){

            destruir(raiz);
            //delete raiz;
            //raiz = nullptr;
        }*/
    }
}

template <typename T>
bool string_map<T>::tieneHijos(Nodo* root){
    for(int i = 0; i < 256; i++){
        if(root->siguientes[i])
            return true;
    }
    return false;
}

template <typename T>
int string_map<T>::size() const{
    return sizeAux(raiz);
}

template <typename T>
int string_map<T>::sizeAux(Nodo* root) const{
    for(int i = 0; i < 256; i++)
    {
        if((root->siguientes)[i] != nullptr){
            if(root -> siguientes[i] -> definicion != nullptr)
                return 1 + sizeAux(root->siguientes[i]);
            else
                return sizeAux(root ->siguientes[i]);
        }
    }
    return 0;
}

template <typename T>
bool string_map<T>::empty() const{
    int i = 0;
    while(i < 256 && !(raiz->siguientes)[i])
        i++;
    return (i == 256);
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& b) {

    if(raiz == nullptr)
    {
        raiz = new Nodo();
        return;
    }

    Nodo* actual = raiz;

    for(int i = 0; i < b.first.size(); i++)
    {
        int letra = int(b.first[i]);
        if(actual->siguientes[letra] == nullptr)
        {
            actual->siguientes[letra] = new Nodo();

        }
        actual = actual ->siguientes[letra];
    }
    if(actual -> definicion != nullptr) delete actual -> definicion;

    actual->definicion = new T(b.second);
}
