#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha
{
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    #if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    #endif
    void incrementar_dia();

  private:
    int mes_, dia_;
};
Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia){}

#if EJ >= 9
bool Fecha::operator==(Fecha o)
{
    return this->dia() == o.dia() && this->mes() == o.mes();
}
#endif

int Fecha::dia()
{
    return dia_;
}
int Fecha::mes()
{
    return mes_;
}
ostream& operator<<(ostream& os, Fecha f)
{
    os << f.dia() << "/" << f.mes();
    return os;
}

void Fecha::incrementar_dia()
{
    dia_++;
    if (dia_ > dias_en_mes(mes_)){
        if (mes_ < 12)
            mes_++;
        else
            mes_ = 1;
        dia_ = 1;
    }
}

// Clase Horario
class Horario
{
public:
    Horario(uint hora, uint minuto);
    uint hora();
    uint min();
    bool operator<(Horario h);
    bool operator>(Horario h);

private:
    uint hora_, minuto_;
};
Horario::Horario(uint hora, uint minuto) : hora_(hora), minuto_(minuto){}

uint Horario:: hora() {
    return hora_;
}

uint Horario::min(){
    return minuto_;
}
ostream& operator<<(ostream& os, Horario h) {
    os << h.hora() << ":" << h.min();
    return os;
}
bool Horario :: operator<(Horario h)
{
    return this->hora() < h.hora() || (this->hora() == h.hora()) && this->min() < h.min();
}
bool Horario :: operator>(Horario h)
{
    return this->hora() > h.hora() || (this->hora() == h.hora()) && this->min() > h.min();
}

// Clase Recordatorio
class Recordatorio{
public:
    Recordatorio(Fecha f, Horario h, string msg);
    Fecha fecha();
    Horario hora();
    string msg();
    bool operator<(Recordatorio r);
    bool operator>(Recordatorio r);
private:
    Fecha f_;
    Horario h_;
    string msg_;
};
Recordatorio :: Recordatorio(Fecha f, Horario h, string msg) : f_(f), h_(h), msg_(msg){}

Fecha Recordatorio :: fecha() {
    return f_;
}

Horario Recordatorio :: hora() {
    return h_;
}

string Recordatorio :: msg() {
    return msg_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.msg() << " @ " << r.fecha() << " " << r.hora();
    return os;
}

bool Recordatorio :: operator<(Recordatorio r)
{
    return this->hora() < r.hora();
}
bool Recordatorio :: operator>(Recordatorio h)
{
    return this->hora() > h.hora();
}

// Ejercicio 14
class Agenda{
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();
private:
    list<Recordatorio> rechoy_;
    Fecha hoy_;


};
Agenda :: Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial){}

list<Recordatorio> Agenda :: recordatorios_de_hoy(){
    return rechoy_;
};

Fecha Agenda :: hoy(){
    return hoy_;
}

void Agenda :: incrementar_dia() {
    hoy_.incrementar_dia();
}

void Agenda :: agregar_recordatorio(Recordatorio rec){
    rechoy_.push_back(rec);
}

ostream& operator<<(ostream& os, Agenda a) {
    list<Recordatorio> recs = a.recordatorios_de_hoy();
    recs.sort();
    os << a.hoy() << endl;
    os << "=====" << endl;
    while (recs.size() > 0) {
        if(recs.front().fecha() == a.hoy())
            os << recs.front() << endl;
        recs.pop_front();
    }
    return os;
}

