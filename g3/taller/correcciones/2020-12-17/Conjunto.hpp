#include "stack"
#include "Conjunto.h"

template <class T>
Conjunto<T>::Conjunto() {
    _raiz = NULL;
}

template <class T>
Conjunto<T>::~Conjunto() {
    _raiz = NULL;
    free(_raiz);
}

template <class T>
void Conjunto<T>::destruir(Nodo* n)
{
    if (n != NULL) {
        destruir(n->izq);
        destruir(n->der);

        delete n;
		n = NULL;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return perteneceAux(_raiz, clave);
}
template <class T>
bool Conjunto<T>::perteneceAux(Nodo* raiz, const T& clave) const {
    if (raiz == NULL)
        return false;
    if (raiz -> valor == clave)
        return true;
    return perteneceAux(raiz->der, clave) + perteneceAux(raiz->izq, clave);
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo *actual = _raiz;
    Nodo *anterior = NULL;

    if(!pertenece(clave)) {
        if (!_raiz) {
            _raiz = new Nodo(clave);
            return;
        } else {
            while (actual != NULL) {
                anterior = actual;

                if (clave < actual->valor)
                    actual = actual->izq;
                else
                    actual = actual->der;
            }

            if (clave < anterior->valor)
                anterior->izq = new Nodo(clave);
            else
                anterior->der = new Nodo(clave);
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    Nodo* temp = _raiz;
    _raiz = NULL;

    reinsertarExcluyendo(temp, clave);

    destruir(temp);
}
template <class T>
void Conjunto<T>::reinsertarExcluyendo(Nodo* root, const T& clave)
{
    if (root != NULL)
    {
        if(root -> valor != clave)
        {
            insertar(root -> valor);
            reinsertarExcluyendo(root -> der, clave);
            reinsertarExcluyendo(root -> izq, clave);
        }
        else
        {
            reinsertarExcluyendo(root -> der, clave);
            reinsertarExcluyendo(root -> izq, clave);
        }
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) const{
    Nodo* root = _raiz;
    Nodo* succ = nullptr;

    while (1)
    {
        if (clave < root->valor)
        {
            succ = root;
            root = root->izq;
        }
        else if (clave > root->valor)
        {
            root = root->der;
        }
        else
        {
            if (root->der)
            {
                root = root -> der;
                while (root->izq)
                    root = root->izq;
                succ = root;
            }
            break;
        }
    }
    return succ -> valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    struct Nodo* actual = _raiz;

    while (actual->izq != NULL)
    {
        actual = actual->izq;
    }
    return(actual->valor);
}

template <class T>
const T& Conjunto<T>::maximo() const {
    struct Nodo* actual = _raiz;

    while (actual->der != NULL)
    {
        actual = actual->der;
    }
    return(actual->valor);
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return cardinalAux(_raiz);
}

template <class T>
unsigned int Conjunto<T>::cardinalAux(Nodo* raiz) const {
    if (raiz == NULL)
        return 0;
    return 1 + cardinalAux(raiz->der) + cardinalAux(raiz->izq);
}
template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}



