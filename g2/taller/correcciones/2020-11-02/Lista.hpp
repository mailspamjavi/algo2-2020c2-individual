#include "Lista.h"

Lista::Lista() {
    primero = NULL;
    ultimo = NULL;
}

Lista::Lista(const Lista& l) : Lista() {
    int i = 0;

    while(this->longitud() > 0)
        this->eliminar(0);

    while (l.longitud() != this->longitud()){
        this->agregarAtras(l.iesimo(i));
        i++;
    }

}
Lista::~Lista() {
    while(this->longitud() > 0)
        this->eliminar(0);
    //free(this);
}

Lista& Lista::operator=(const Lista& aCopiar) {
    int i = 0;

    while(this->longitud() != 0)
        this->eliminar(0);

    while (aCopiar.longitud() != this->longitud()){
        this->agregarAtras(aCopiar.iesimo(i));
        i++;
    }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    struct Nodo* newNodo = new Nodo;
    newNodo -> valor = elem;
    newNodo -> siguiente = primero;
    newNodo -> anterior = NULL;

    if( primero != NULL)
        primero->anterior = newNodo;
    else
        ultimo = newNodo;

    primero = newNodo;
}

void Lista::agregarAtras(const int& elem) {
    struct Nodo* newNodo = new Nodo;
    newNodo -> valor = elem;
    newNodo -> siguiente = NULL;
    newNodo -> anterior = ultimo;

    if(ultimo != NULL)
        ultimo->siguiente = newNodo;
    else
        primero = newNodo;

    ultimo = newNodo;
}

void Lista::eliminar(Nat i) {
    int posicion = 0;
    struct Nodo* nodoABorrar = primero;

    while(posicion != i){
        nodoABorrar = nodoABorrar->siguiente;
        posicion++;
    }

    if(nodoABorrar == ultimo)
        ultimo = nodoABorrar->anterior;
    else
        (nodoABorrar->siguiente) -> anterior = nodoABorrar->anterior;

    if(nodoABorrar == primero)
        primero = nodoABorrar->siguiente;
    else
        (nodoABorrar->anterior) -> siguiente = nodoABorrar->siguiente;

    nodoABorrar = NULL;
    free(nodoABorrar);
    //delete nodoABorrar;
}

int Lista::longitud() const {
    int fin = 0;
    struct Nodo *contador = primero;
        while (contador != NULL) {
            contador = contador->siguiente;
            fin++;
        }
    return fin;
}

const int& Lista::iesimo(Nat i) const {
    int posicion = 0;
    struct Nodo* nodoDevolver = primero;

    while(posicion != i){
        nodoDevolver = nodoDevolver->siguiente;
        posicion++;
    }
    return  nodoDevolver -> valor;
}

int& Lista::iesimo(Nat i) {
    int posicion = 0;
    struct Nodo* nodoDevolver = primero;

    while(posicion != i){
        nodoDevolver = nodoDevolver->siguiente;
        posicion++;
    }
    return  nodoDevolver -> valor;
}

void Lista::mostrar(ostream& o) {
    struct Nodo* nodoPrint = primero;
    while(nodoPrint != NULL){
        o << nodoPrint -> valor;
        if(nodoPrint->siguiente == NULL)
            o;
        else{
            o << " <--> ";
        }
        nodoPrint = nodoPrint->siguiente;
    }

}
